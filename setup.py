import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='crutils',
    version='0.0.3',
    author='gbilder',
    author_email='gbilder@crosref.org',
    description='Testing installation of CR Package',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/crossref/labs/airflow-daily-new-doi-count-task',
    project_urls = {
        "Bug Tracker": "https://gitlab.com/crossref/labs/airflow-daily-new-doi-count-task/issues"
    },
    license='MIT',
    packages=['crutils'],
    install_requires=['requests'],
)