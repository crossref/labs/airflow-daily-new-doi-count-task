import click
import logging
from distrunner.distrunner import DistRunner


def run_distributed_task():
    # launch the distributed task
    with DistRunner(
        workers=2,
        python_version="3.10",
        repo="https://gitlab.com/crossref/labs/task-test.git",
        entry_module="task",
        entry_point="entry_point",
        requirements_file="requirements.txt",
        local=True,
        retries=3,
        worker_memory=16384,
        worker_cpus=4096,
        verbose_workers=True,
        task_role_arn="arn:aws:iam::860842417165:role/"
        "ECS_Launcher_Permission_Role",
        on_airflow=False,
        on_aws=False,
    ) as dr:
        logging.basicConfig(level=logging.INFO)

        dr.run()


@click.group()
def cli():
    pass


@click.command()
def launch():
    """Launch the cluster"""
    run_distributed_task()


if __name__ == "__main__":
    # test_geoff()
    run_distributed_task()
