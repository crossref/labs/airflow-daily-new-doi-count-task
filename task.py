import whatismyip
from crutils.crapi import MAILTO
from datetime import datetime, timedelta

#MAILTO = "FOO"


def generate_dates(start_date, end_date):
    """Generate a list of ISO date strings  between start_date and end_date with the format YYYY-MM-DD"""
    dates = []
    while start_date <= end_date:
        dates.append(start_date.strftime("%Y-%m-%d"))
        start_date += timedelta(days=1)
    return dates

    

def entry_point(cldr):
    # this builds a lazy task graph and does not execute the function
    results = cldr.client.map(print_ip, generate_dates(datetime(2019, 1, 1), datetime(2019, 1, 10)))

    # this executes the graph
    outcome = cldr.client.gather(results)

    print(outcome)


def print_ip(x):
    return f"{MAILTO} is getting data for date {x} from IP address {whatismyip.whatismyip()}"



